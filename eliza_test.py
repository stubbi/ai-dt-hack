# https://machinelearningmastery.com/develop-a-deep-learning-caption-generation-model-in-python/
#

BEAUTY = True
from os import listdir

from pickle import load
from numpy import argmax
from keras.preprocessing.sequence import pad_sequences
from keras.applications.vgg16 import VGG16
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.models import Model
from keras.models import load_model
 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


# extract features from each photo in the directory
def extract_features(filename, model):

	# load the photo
	image = load_img(filename, target_size=(224, 224))
	# convert the image pixels to a numpy array
	image = img_to_array(image)
	# reshape data for the model
	image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
	# prepare the image for the VGG model
	image = preprocess_input(image)
	# get features
	feature = model.predict(image, verbose=0)
	return feature
 
# map an integer to a word
def word_for_id(integer, tokenizer):
	for word, index in tokenizer.word_index.items():
		if index == integer:
			return word
	return None
 
# generate a description for an image
def generate_desc(model, tokenizer, photo, max_length):
	# seed the generation process
	in_text = ''
	# iterate over the whole length of the sequence
	for i in range(max_length):
		# integer encode input sequence
		sequence = tokenizer.texts_to_sequences([in_text])[0]
		# pad input
		sequence = pad_sequences([sequence], maxlen=max_length)
		# predict next word
		yhat = model.predict([photo,sequence], verbose=0)
		# convert probability to integer
		yhat = argmax(yhat)
		# map integer to word
		word = word_for_id(yhat, tokenizer)
		# stop if we cannot map the word
		if word is None:
			print("none: " + yhat)   
			break        
		# stop if we predict the end of the sequence
		if word == 'endseq':
			break
		# append as input for generating the next word
		in_text += ' ' + word

	return in_text
 
# load the tokenizer
tokenizer = load(open('eliza_tokenizer.pkl', 'rb'))
tokenizerDE = load(open('eliza_DE_tokenizer.pkl', 'rb'))

# pre-define the max sequence length (from training)
max_length = 75   # RIS: BEWARE THIS NUMBER IS NOT GENERIC!!!!!!!
max_lengthDE = 74   # RIS: BEWARE THIS NUMBER IS NOT GENERIC!!!!!!!

# load the models
cnnmodel = VGG16()
cnnmodel.layers.pop()
cnnmodel = Model(inputs=cnnmodel.inputs, outputs=cnnmodel.layers[-1].output)

model = load_model('eliza-brain-test.h5')
modelDE = load_model('eliza-DE-brain-test.h5')

# load and prepare the photograph

german = ''
english = ''

directory = 'eliza_dataset/test'
for name in listdir(directory):
    filename = directory + '/' + name
    photo = extract_features(filename, cnnmodel)
    # generate description
    description = generate_desc(model, tokenizer, photo, max_length)
    descriptionDE = generate_desc(modelDE, tokenizerDE, photo, max_lengthDE)

    if BEAUTY:
        # show some beauty
        fig = plt.figure()
        img=mpimg.imread(filename)
        plt.imshow(img)
        plt.title(description, fontsize=10, color = 'red')
        plt.xlabel(descriptionDE, fontsize=10, color = 'green')
        plt.show()
        plt.close(fig)
        #fig.suptitle(description, fontsize=10, fontweight='bold', color = 'red')
        #fig = plt.figure()
        #fig.suptitle(descriptionDE, fontsize=10, fontweight='bold',  color = 'green')
        #plt.imshow(img)
    # and plain text
    print (filename)
    #files
    german = german + name + ' ' + descriptionDE + '\r\n'
    english = english + name + ' ' + description + '\r\n'
    
# files
germanfile = open("eliza_dataset/descriptions_ger.txt","w+")
englishfile = open("eliza_dataset/descriptions_en.txt","w+")
germanfile.write(german)
englishfile.write(english)
germanfile.close()
englishfile.close()